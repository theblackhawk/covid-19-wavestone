import pandas as pd
import tqdm as tqdm
from JsonParser import JsonParser
from TextCleaner import clean_txt

json_parent_folder = "/media/hdd1/covid19_kaggle/origin_data"
output_csv_folder = "/media/hdd1/covid19_kaggle/cleaned_data"

if __name__ == "__main__":
    #Creating csv file from json files
    #instance = JsonParser(json_parent_folder=json_parent_folder,output_csv_folder=output_csv_folder)
    clean_txt(output_csv_folder+ '/cleaned_output.csv','~/clean_txt_data.csv')


