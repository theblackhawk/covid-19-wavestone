#!/bin/bash
kaggle datasets download allen-institute-for-ai/CORD-19-research-challenge --force
#mv CORD-19-research-challenge.zip /media/hdd1/covid19_kaggle/origin_data
#unzip /media/hdd1/covid19_kaggle/CORD-19-research-challenge.zip
mv CORD-19-research-challenge.zip ~/test_kaggle
unzip ~/test_kaggle/CORD-19-research-challenge.zip
#python3 ~/covid19-risk-factors/src/DataCleaner.py