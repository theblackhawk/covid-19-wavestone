# -*- coding: utf-8 -*-
import string
import nltk
from nltk.tree import Tree
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from bs4 import BeautifulSoup
import unidecode
import re
import pandas as pd
from tqdm import tqdm
tqdm.pandas()





class TextCleaner():
    """Provides ways of preprocessing text data."""

    def __init__(self):
        self.punctuation = list(string.punctuation)
        self.stop_words = stopwords.words('english') + self.punctuation + ['\\n'] + ['quot'] + ['\n']
    
    def lowercase(self,text):
        """Preprocess a whole raw document.
        Args:
            text (str): Raw string of text.
        Return:
            Text lowercase.
        """
        return text.lower()
    
    def strip_html_tags(self,text):
        """Preprocess a whole raw document.
        Args:
            text (str): Raw string of text.
        Return:
            Text without html tags ].
        """
        soup = BeautifulSoup(text, "html.parser")
        stripped_text = soup.get_text(separator=" ")
        return stripped_text
    
    def remove_accented_chars(self,text):
        """Preprocess a whole raw document.
            Args:
                text (str): Raw string of text.
            Return:
                Text without accents : Exple café => cafe].
        """
        text = unidecode.unidecode(text)
        return text


    def expandContractions(self,text):
        cList={}
        with open("contractions.txt") as f:
            for line in f:
                (key,val) = line.split(':')
                cList[key] = val

        c_re = re.compile('(%s)' % '|'.join(cList.keys()))
        def replace(match):
            return cList[match.group(0)]
        return c_re.sub(replace, text).replace('\n','')

    def remove_stop_words(self,text):
        text = ' '.join([word for word in text.split() if word not in self.stop_words])
        return text



# text_cleaner = TextCleaner()
# print(text_cleaner.lowercase('I Here DA'))
# print(text_cleaner.strip_html_tags('I am heRe in this web </> kjhsfk'))
# print(text_cleaner.remove_accented_chars(' du café'))
# print(text_cleaner.expandContractions("He didn't do it"))

def clean_txt(input_file,output_file):
    """Preprocess a dataframe
            Args:
                input_file : input dataframe
                output_file : output dataframe
            Return:
    """
    input_data = pd.read_csv(input_file)
    input_data.abstract = input_data.abstract.fillna(' ')
    input_data.text = input_data.text.fillna(' ')
    input_data['content'] = input_data['text'] + input_data['abstract']
    input_data = input_data[['paper_id', 'title', 'authors', 'affiliations','bibliography', 'raw_authors', 'raw_bibliography', 'content']]
    TCleaner=TextCleaner()
    print('Removing stop words ...')
    input_data.content = input_data.content.apply(lambda x : TCleaner.remove_stop_words(x))
    print('Removing html tags ...')
    input_data.content = input_data.content.progress_apply(lambda x : TCleaner.strip_html_tags(x))
    print('Removing accented chars ...')
    input_data.content = input_data.content.progress_apply(lambda x : TCleaner.remove_accented_chars(x))
    print('Expanding contractions ')
    input_data.content = input_data.content.progress_apply(lambda x : TCleaner.expandContractions(x))
    print('Lowercase ...')
    input_data.content = input_data.content.progress_apply(lambda x : TCleaner.lowercase(x))

    input_data.to_csv(output_file, index=False)

