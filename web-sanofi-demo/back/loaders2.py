import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import os
import re
import errno
import json
import pickle
import glob
import multiprocessing
from time import time  # To time our operations

total_dataframe = pd.DataFrame()
models = glob.glob("*.pickle")
#models = glob.glob('/kaggle/working/biorxiv_medrxiv.pickle')
# print('models via glob===>'+str(models))
for individual_model in models:
    print(individual_model)
    # open a file, where you stored the pickled data
    file = open(individual_model, 'rb')

    # dump information to that file
    data = pickle.load(file)

    # close the file
    file.close()

    #print('Showing the pickled data:')
    my_df = pd.DataFrame(data)
    #print(my_df.shape)
    my_df = my_df.replace(r'^\s*$', np.nan, regex=True)
    total_dataframe = total_dataframe.append(my_df, ignore_index=True)
    #print('Null per column')
    #print(my_df.isnull().sum())

total_dataframe = total_dataframe.drop(['abstract_tripples', 'text_tripples','entities','key_phrases'], axis=1)
#print('Total data in dataframe')
#print(total_dataframe.shape)
#total_dataframe.head()
tf_df = pd.DataFrame()
tf_df['merged_text'] = total_dataframe['title'].astype(str) +  total_dataframe['abstract'].astype(str) +  total_dataframe['text'].astype(str)
tf_df['paper_id'] = total_dataframe['paper_id']
print(tf_df.head())
