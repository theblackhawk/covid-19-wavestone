import os
from flask import Flask, render_template, request
from motor import ResearchMotor
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import os
import re
import errno
import json
import pickle
import glob
import multiprocessing
from time import time
from motor import TextSummarizer
import random


app = Flask(__name__)

RESEARCH_PAPERS = ''

def load_dataframe():
    total_dataframe = pd.DataFrame()
    models = glob.glob("*.pickle")

    #models = glob.glob('/kaggle/working/biorxiv_medrxiv.pickle')
    # print('models via glob===>'+str(models))
    for individual_model in models:
        print(individual_model)
        # open a file, where you stored the pickled data
        file = open(individual_model, 'rb')

        # dump information to that file
        data = pickle.load(file)

        # close the file
        file.close()

        #print('Showing the pickled data:')
        my_df = pd.DataFrame(data)
        #print(my_df.shape)
        my_df = my_df.replace(r'^\s*$', np.nan, regex=True)
        total_dataframe = total_dataframe.append(my_df, ignore_index=True)
        #print('Null per column')
        #print(my_df.isnull().sum())

    total_dataframe = total_dataframe.drop(['abstract_tripples', 'text_tripples','entities','key_phrases'], axis=1)
    #print('Total data in dataframe')
    #print(total_dataframe.shape)
    #total_dataframe.head()
    tf_df = pd.DataFrame()
    tf_df['merged_text'] = total_dataframe['title'].astype(str) +  total_dataframe['abstract'].astype(str) +  total_dataframe['text'].astype(str)
    tf_df['paper_id'] = total_dataframe['paper_id']
    print(tf_df.head())
    return tf_df

@app.route("/")
def hello():
    return render_template("index.html")

@app.route("/search", methods = ['GET', 'POST'])
def search():
    global cache_dataframe
    global TS
    # Tous les paper id avec le text
    if request.method == 'POST':
        print(list(request.form.keys()))

        if('simple_search' in list(request.form.keys())):
            keywords       = request.form['keywords']
            date           = request.form['date']
            related_covid  = 'on'
            related = {'on' : True, 'off': False}
            result = RESEARCH_PAPERS.search_papers(SearchTerms=keywords, covid_related = related[related_covid])
            result.results = result.results[~result.results['url'].str.contains("http://doi.org/10.1101/2020.04.17.20069708")]
            result.results["summary"] = pd.DataFrame(["" for i in range(len(result.results))], columns=['summary'])
            print(keywords)
            print(date)
            print(related_covid)
            print(result.results.columns)
            #Actualiser article en ajoutant le résumé
            articles = result.results[["title", "abstract", "Score", "sha", "url", "summary"]].values
        else:        
            keywords       = request.form['keywords']
            date           = request.form['date']
            related_covid  = 'on'
            related = {'on' : True, 'off': False}
            #Croiser avec sha et df text
            result = RESEARCH_PAPERS.search_papers(SearchTerms=keywords, covid_related = related[related_covid])
            result.results = result.results[~result.results['url'].str.contains("http://doi.org/10.1101/2020.04.17.20069708")]
            enrich_result = cache_dataframe.merge(result.results, on='sha', how='inner')
            #Boucler sur la liste pour résumer
            enrich_result['summary'] = pd.DataFrame(list(map(TS.summarize, enrich_result['merged_text'].values)), columns=['summary'])
            print(keywords)
            print(date)
            print(related_covid)
            print(result.results.columns)

            ids      = result.results["cord_uid"].values
            print(ids)
            #Actualiser article en ajoutant le résumé
            articles = enrich_result[["title", "abstract", "Score", "sha","url", "summary"]].values

        return render_template("index.html",articles = articles)

    return "Ok"


if __name__ == "__main__":
    random.seed(42)
    metadata = ResearchMotor.load_metadata()
    RESEARCH_PAPERS = ResearchMotor(metadata)
    #Load the dataframe
    cache_dataframe = load_dataframe()
    cache_dataframe.columns = ['merged_text', 'sha']
    TS = TextSummarizer()
    app.run(host='0.0.0.0', port=5420)
