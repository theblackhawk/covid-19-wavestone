
import pickle
import re
import time
from pathlib import Path, PurePath

import ipywidgets as widgets
import numpy as np
import pandas as pd
import requests
from IPython.display import display, clear_output
from rank_bm25 import BM25Okapi
from requests import HTTPError
from cord.core import render_html, show_common, describe_dataframe, is_kaggle, CORD_CHALLENGE_PATH, \
    find_data_dir, SARS_DATE, SARS_COV_2_DATE, listify
from cord.dates import add_date_diff
from cord.jsonpaper import load_json_paper, PDF_JSON, PMC_JSON, \
    get_json_paths
from cord.text import preprocess, shorten, summarize
from cord.vectors import show_2d_chart, similar_papers
from cord  import ResearchPapers

from transformers import *
from summarizer   import Summarizer

class ResearchMotor(ResearchPapers):

    def __init__(self, metadata, bm25_index = None, data_dir='data', index='abstract',view='html'):
        ResearchPapers.__init__(self, metadata, bm25_index = None, data_dir='data', index='abstract',view='html')

    def search_papers(self, SearchTerms, covid_related = True):

        search_results = self.search(SearchTerms, num_results=10, view='html', covid_related=covid_related)
        return search_results

class TextSummarizer():
    # Loading the Scibert Model
    def __init__(self):
        self.scibert_link  = 'allenai/scibert_scivocab_uncased'
        self.sci_config    = AutoConfig.from_pretrained(self.scibert_link)
        self.sci_config.output_hidden_states = True
        self.sci_tokenizer = AutoTokenizer.from_pretrained(self.scibert_link)
        self.sci_model     = AutoModel.from_pretrained(self.scibert_link, config = self.sci_config)

        # Setting up Bert-Extractive-Summarizer to use SciBert
        self.sci_model = Summarizer(custom_model = self.sci_model, custom_tokenizer = self.sci_tokenizer)



    def summarize(self, text):
      # Bert-Extractive-Summarizer max-limit exceeded
        if len(text) >= 1000000:
            return False

        ratio   = 0.10
        summary = self.sci_model(text, ratio = ratio)
        return summary

if __name__ == "__main__":
    TS = TextSummarizer()
    a = TS.summarize("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
    print(a)
