---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.4.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas as pd
import numpy as np
```

```python
! pip install COVID19Py
```

##### Covid19py is a wrapper for Coronavirus Tracker API that uses john hopkins university for worldwide data as backend and csbs data for the USA
Documentation on https://github.com/Kamaropoulos/COVID19Py


### Test COVID19Py

```python
import COVID19Py
covid19 = COVID19Py.COVID19()
#JHU is used by default as a backend, add data_source="csbs" as parameter above for the other backend
```

```python
location = covid19.getLocationByCountryCode("FR", timelines=True)
```

```python
location
```

```python
location = covid19.getLocationByCountryCode("FR", timelines=False)
```

```python
location
```

```python

```
