---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.4.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas as pd
import numpy as np
```

```python
! pip install COVID19Py
```

##### Covid19py is a wrapper for Coronavirus Tracker API that uses john hopkins university for worldwide data as backend and csbs data for the USA
Documentation on https://github.com/Kamaropoulos/COVID19Py


### Test COVID19Py

```python
import COVID19Py
covid19 = COVID19Py.COVID19()
```

```python
location2 = covid19.getLocationByCountryCode("FR", timelines=True)
```

```python
location2[0]['timelines']['confirmed']['timeline']['2020-01-22T00:00:00Z']
```

```python

```

```python

```

```python
location = covid19.getLocationByCountryCode("FR", timelines=True)
```

```python
location
```

```python
df = pd.DataFrame([], columns=["id", "country", "country_code", "country_population", "province", "timeline", "latitude", "longitude", "confirmed", "deaths", "recovered"])
```

```python
location[2]
```

```python
len(location)
```

## Data Parser

```python
import COVID19Py

def parse_data(code_pays, timeline=False):
    """
    Country code can be found in https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
    Not all coutries are available
    TODO: Add exceptions bloc to catch "country not available" error
    """
    df = pd.DataFrame([], columns=["id", "country", "country_code", "country_population", "province", "timeline", "latitude", "longitude", "confirmed", "deaths", "recovered"])
    covid19 = COVID19Py.COVID19()
    data = covid19.getLocationByCountryCode(code_pays, timelines=timeline)
    if(timeline == False):
        for i in range(len(data)):
            idd = data[i]['id']
            data_country = data[i]['country']
            country_code = data[i]['country_code']
            country_population = data[i]['country_population']
            province = data[i]['province']
            timeline = data[i]['last_updated']
            long = data[i]['coordinates']['longitude']
            lat = data[i]['coordinates']['latitude']
            confirmed = data[i]['latest']['confirmed']
            deaths = data[i]['latest']['deaths']
            recovered = data[i]['latest']['recovered']
            df.loc[len(df)] = [idd, data_country, country_code, country_population, province, timeline, lat, long, confirmed, deaths, recovered]
    else:
        for i in range(len(data)):
            idd = data[i]['id']
            data_country = data[i]['country']
            country_code = data[i]['country_code']
            country_population = data[i]['country_population']
            province = data[i]['province']
            long = data[i]['coordinates']['longitude']
            lat = data[i]['coordinates']['latitude']
            timeline_all = list(data[i]['timelines']['confirmed']['timeline'].keys())
            for j in range(len(timeline_all)):
                try:
                    date = timeline_all[j]
                except KeyError:
                    pass
                try:
                    confirmed = data[i]['timelines']['confirmed']['timeline'][timeline_all[j]]
                except KeyError:
                    confirmed = float("nan")
                try:
                    deaths = data[i]['timelines']['deaths']['timeline'][timeline_all[j]]
                except KeyError:
                    deaths = float("nan")
                try:
                    recovered = data[i]['timelines']['recovered']['timeline'][timeline_all[j]]
                except KeyError:
                    recovered = float("nan")

                df.loc[len(df)] = [idd, data_country, country_code, country_population, province, date, lat, long, confirmed, deaths, recovered]
            
    return df
```

```python
def get_delta(df):
    # Get number of new case, new death and new recovered. Need to group by province before use
    delta_confirmed = [0]
    delta_death = [0]
    delta_recovered = [0]
    confirmed = list(df['confirmed'])
    deaths = list(df['deaths'])
    recovered = list(df['recovered'])
    for i in range(1, len(df)):
        try:
            delta_confirmed.append(confirmed[i] - confirmed[i-1])
            delta_death.append(deaths[i] - deaths[i-1])
            delta_recovered.append(recovered[i] - recovered[i-1])
        except:
            pass
    df = df.assign(delta_confirmed = delta_confirmed)
    df = df.assign(delta_death = delta_death) 
    df = df.assign(delta_recovered = delta_recovered) 
    return df
```

# Analysis per country


### Info confinement par pays

```python
info_per_country = pd.read_csv("./Data/covid19countryinfo.csv")
```

```python
info_per_country[info_per_country['alpha2code'] == "FR"]["schools"]
```

```python
info_per_country.columns
```

```python
keep_columns = ['country', 'alpha2code', 'quarantine', 'schools', 'publicplace', 'gatheringlimit', 'gathering', 'nonessential']
info_per_country = info_per_country.loc[:, info_per_country.columns.isin(keep_columns)]
```

### Nombre de test par pays

```python
# Source données : https://github.com/owid/covid-19-data
test_raw = pd.read_csv("./Data/covid-testing-all-observations.csv")
```

```python
keep_columns_test = ['Entity', 'Date', 'Cumulative total', 'Daily change in cumulative total', 'Cumulative total per thousand']
test_raw = test_raw.loc[:, test_raw.columns.isin(keep_columns_test)]
```

```python
test_raw
```

#### Preprocess Entity field

```python
def get_country(country):
    return country.split('-')[0].strip()

test_raw['Entity'] = test_raw['Entity'].apply(get_country)
```

```python
test_raw
```

```python
pd.set_option('display.max_row', 1000)
test_raw.Entity.unique()
```

### Taux de mortalité vs age median du pays

```python
mortalite_age = pd.read_csv("./Data/case-fatality-rate-of-covid-19-vs-median-age.csv")
```

```python
keep_columns3 = ['Code', 'Date', 'Case fatality rate of COVID-19 (%)', 'Median Age', 'Total confirmed deaths due to COVID-19 (deaths)']
mortalite_age = mortalite_age.loc[:, mortalite_age.columns.isin(keep_columns3)]
```

### France DATA

```python
france_data = parse_data("FR", timeline=True)
```

```python
france_data
```

```python
france_data_aggregated = france_data.groupby('timeline').agg(confirmed = ("confirmed", "sum"), deaths = ("deaths", "sum"), recovered = ("recovered", "sum"))
```

```python
france_data_aggregated = get_delta(france_data_aggregated)
```

```python
france_data_aggregated
```

```python
info_france = info_per_country[info_per_country["alpha2code"] == 'FR']
info_france
```

```python
test_france = test_raw[test_raw['Entity'] == 'France'].reset_index()
test_france
```

# --------------------------------------------------------------

```python
import datetime
x = datetime.datetime(2020, 1, 22)
print(x)
```

```python
test_france.loc[0]['Cumulative total']
```

```python
mortalite_age_france = mortalite_age[mortalite_age['Code'] == 'FRA']
mortalite_age_france
```

### Germany Data

```python
germany_data = parse_data("DE", timeline=True)
```

```python
germany_data
```

```python
info_germany = info_per_country[info_per_country["alpha2code"] == 'DE']
info_germany
```

```python
test_germany = test_raw[test_raw['Entity'] == 'Germany']
test_germany
```

### Korea Data

```python
korea_data = parse_data("KR", timeline=True)
```

```python
korea_data
```

```python
info_korea = info_per_country[info_per_country["alpha2code"] == 'KR']
info_korea
```

```python
test_korea = test_raw[test_raw['Entity'] == 'South Korea']
test_korea
```

### Spain data

```python
spain_data = parse_data("ES", timeline=True)
```

```python
spain_data
```

```python
info_spain = info_per_country[info_per_country["alpha2code"] == 'ES']
info_spain
```

```python
test_spain = test_raw[test_raw['Entity'] == 'Spain']
test_spain
```

### Italy data

```python
italy_data = parse_data("IT", timeline=True)
```

```python
italy_data
```

```python
info_italy = info_per_country[info_per_country["alpha2code"] == 'IT']
info_italy
```

```python
test_italy = test_raw[test_raw['Entity'] == 'Italy']
test_italy
```

### UK Data

```python
uk_data = parse_data("GB", timeline=True)
```

```python
uk_data
```

```python
info_uk = info_per_country[info_per_country["alpha2code"] == 'GB']
info_uk
```

```python
test_uk = test_raw[test_raw['Entity'] == 'United Kingdom']
test_uk
```

### China data

```python
china_data = parse_data("CN", timeline=True)
```

```python
china_data
```

```python
info_china = info_per_country[info_per_country["alpha2code"] == 'CN'].drop([32])
info_china
```

```python
test_china = test_raw[test_raw['Entity'] == 'China']
test_china
```

### USA data

```python
usa_data = parse_data("US", timeline=True)
```

```python
usa_data
```

```python
info_usa = info_per_country[info_per_country["alpha2code"] == 'US']
info_usa
```

```python
test_usa = test_raw[test_raw['Entity'] == 'United States']
test_usa.drop(test_usa.loc[3475:3567].index, inplace=True)
test_usa
```

```python

```

```python

```
